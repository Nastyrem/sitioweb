<?php

require 'app/models/Cliente.php';
use UPT\Cliente;

class ClienteController
{
    function login(){
        require 'app/views/login.php';
    }

    function insertarRegistros()
    {
        $cliente = new Cliente();
        $cliente->nombre=$_POST["nombre"];
        $cliente->apellidoP=$_POST["ap_p"];
        $cliente->apellidoM=$_POST["ap_m"];
        $cliente->edad=$_POST["edad"];
        $cliente->correo=$_POST["correo"];
        $cliente->contrasenia=$_POST["contra"];
        $cliente->genero=$_POST["genero"];
        $cliente->registrar();

        $status = "Registro completo";
        echo $status;
    }

    public function acceso(){
        $correo = $_POST["correo"];
        $contra = $_POST["contra"];

        $verificar = Cliente::acceso($correo, $contra);
        if(!$verificar){
            echo"Datos incorrectos";
        }else{

        }

    }

}