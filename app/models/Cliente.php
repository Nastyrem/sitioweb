<?php


namespace UPT;

require 'app/models/Conexion.php';
use UPT\Conexion;


class Cliente extends Conexion
{
    public $id;
    public $nombre;
    public $apellidoP;
    public $apellidoM;
    public $correo;
    public $edad;
    public $contrasenia;
    public $genero;

    public function __construct()
    {
        parent::__construct();
    }

    function registrar($correo, $contra){
        $pre = mysqli_prepare($this->con, "INSERT INTO clientes (nombre, ap_p, ap_m, edad, correo, contrasenia, genero) VALUES (?,?,?,?,?,?,?)");
        $pre->bind_param("sssisss", $this->nombre, $this->apellidoP, $this->apellidoM, $this->edad, $this->correo, $this->contrasenia, $this->genero);
        $pre->execute();
    }

    static function acceso(){
        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->con, "SELECT * FROM clientes WHERE correo=? AND contrasenia=?");
        $pre->bind_param("ss", $correo, $contra );

    }

}