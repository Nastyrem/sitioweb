<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/51a9df7ed4.js" crossorigin="anonymous"></script>
    <title>Registro</title>
</head>
<body>
    <h1 align="center">Registro de usuario</h1>
<center>
    <form method="post" action="index.php?controller=Cliente&action=insertarRegistros">
        <i class="fas fa-user-alt"></i>
        <input type="text" placeholder="Nombre" name="nombre">
        <br>
        <br>
        <i class="fas fa-user-alt"></i>
        <input type="text" placeholder="Apellido Paterno" name="ap_p">
        <br>
        <br>
        <i class="fas fa-user-alt"></i>
        <input type="text" placeholder="Apellido Materno" name="ap_m">
        <br>
        <br>
        <i class="fas fa-percentage"></i>
        <input type="number" placeholder="Edad" name="edad">
        <br>
        <br>
        <i class="fas fa-envelope"></i>
        <input type="email" placeholder="Correo" name="correo">
        <br>
        <br>
        <i class="fas fa-key"></i>
        <input type="password" placeholder="Contraseña" name="contra">
        <br>
        <br>
        <div class="inputContenedor">
            <i class="fas fa-male"></i>
            <input type="radio" name="genero" value="M">Masculino:p
            <i class="fas fa-female"></i>
            <input type="radio" name="genero" value="F">Femenino:p
        </div>
        <br>
        <br>
        <input type="submit" value="Registrar">
    </form>
</center>

</body>
</html>